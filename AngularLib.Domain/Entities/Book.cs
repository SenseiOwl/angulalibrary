﻿using AngularLib.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class Book
    {
        public PublicationType Type { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        public int YearOfPublishing { get; set; }
        public int YearOfWriting { get; set; }
        
        public virtual ICollection<PublisherBook> Publisher { get; set; }      
        public virtual ICollection<AuthorBook> Authors { get; set; }


        public Book()
        {
            Type = PublicationType.Book;
            Publisher = new List<PublisherBook>();
            Authors = new List<AuthorBook>();
        }
    }
}
