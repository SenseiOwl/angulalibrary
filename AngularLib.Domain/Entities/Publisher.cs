﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfOpening { get; set; }

        public virtual ICollection<PublisherBook> Books { get; set; }
        public virtual ICollection<PublisherNewspaper> Newspapers { get; set; }
        public virtual ICollection<PublisherMagazine> Magazines { get; set; }


        public Publisher()
        {
            Books = new List<PublisherBook>();
            Newspapers = new List<PublisherNewspaper>();
            Magazines = new List<PublisherMagazine>();
        }
    }
}
