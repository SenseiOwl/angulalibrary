﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfBirth { get; set; }
        public int? YearOfDeath { get; set; }

        public virtual ICollection<AuthorBook> Books { get; set; }

        public Author()
        {
            Books = new List<AuthorBook>();
        }
    }
}
