﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class PublisherBook
    {
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }
}
