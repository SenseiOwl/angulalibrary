﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class PublisherMagazine
    {
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public int MagazineId { get; set; }
        public Magazine Magazine { get; set; }
    }
}
