﻿using AngularLib.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class Newspaper
    {
        public PublicationType Type { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        public int YearOfPublishing { get; set; }

        public virtual ICollection<PublisherNewspaper> Publisher { get; set; }

        public Newspaper()
        {
            Type = PublicationType.Newspaper;

            Publisher = new List<PublisherNewspaper>();
        }
    }
}
