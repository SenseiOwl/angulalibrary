﻿using System;
using System.Collections.Generic;
using System.Text;


namespace AngularLib.Domain.Enums
{
    public enum PublicationType
    {
        Book = 0,
        Magazine = 1,
        Newspaper = 2
    }
    
}
