﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class ShortAuthorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
