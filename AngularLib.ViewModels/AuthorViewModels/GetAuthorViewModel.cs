﻿using AngularLib.ViewModels.BookViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class GetAuthorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfBirth { get; set; }
        public int? YearOfDeath { get; set; }

        public ICollection<ShortBookViewModel> Books { get; set; }

        public GetAuthorViewModel()
        {
            Books = new List<ShortBookViewModel>();
        }
        
    }
}
