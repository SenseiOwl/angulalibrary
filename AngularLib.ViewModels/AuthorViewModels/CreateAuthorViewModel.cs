﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class CreateAuthorViewModel
    {
        public string Name { get; set; }
        public int YearOfBirth { get; set; }
        public int? YearOfDeath { get; set; }

    }
}
