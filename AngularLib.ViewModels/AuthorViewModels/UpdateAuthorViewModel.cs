﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class UpdateAuthorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfBirth { get; set; }
        public int? YearOfDeath { get; set; }

    }
}
