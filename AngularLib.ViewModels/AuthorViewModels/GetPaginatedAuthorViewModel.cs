﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class GetPaginatedAuthorViewModel
    {
        public ICollection<GetAuthorViewModel> Authors { get; set; }
        public PageInfo PageInfo { get; set; }

        public GetPaginatedAuthorViewModel()
        {
            Authors = new List<GetAuthorViewModel>();
        }
    }
}
