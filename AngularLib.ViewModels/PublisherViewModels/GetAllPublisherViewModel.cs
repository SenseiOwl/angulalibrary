﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.PublisherViewModels
{
    public class GetAllPublisherViewModel
    {
        public ICollection<GetPublisherViewModel> Publishers { get; set; }

        public GetAllPublisherViewModel()
        {
            Publishers = new List<GetPublisherViewModel>();
        }
    }
}
