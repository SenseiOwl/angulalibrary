﻿using AngularLib.ViewModels.BookViewModels;
using AngularLib.ViewModels.MagazineViewModels;
using AngularLib.ViewModels.NewspaperViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.PublisherViewModels
{
    public class CreatePublisherViewModel
    {
        public string Name { get; set; }
        public int YearOfOpening { get; set; }

        public ICollection<ShortBookViewModel> Books { get; set; }
        public ICollection<ShortNewspaperViewModel> Newspapers { get; set; }
        public ICollection<ShortMagazineViewModel> Magazines { get; set; }

        public CreatePublisherViewModel()
        {
            Books = new List<ShortBookViewModel>();
            Newspapers = new List<ShortNewspaperViewModel>();
            Magazines = new List<ShortMagazineViewModel>();
        }
    }
}
