﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.PublisherViewModels
{
    public class ShortPublisherViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
