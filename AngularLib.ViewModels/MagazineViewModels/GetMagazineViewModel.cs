﻿using AngularLib.ViewModels.PublisherViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.MagazineViewModels
{
    public class GetMagazineViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfPublishing { get; set; }

        public ICollection<ShortPublisherViewModel> Publisher { get; set; }

        public GetMagazineViewModel()
        {
            Publisher = new List<ShortPublisherViewModel>();
        }
    }
}
