﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.MagazineViewModels
{
    public class ShortMagazineViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
