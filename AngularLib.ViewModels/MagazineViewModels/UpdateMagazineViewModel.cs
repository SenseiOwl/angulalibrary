﻿using AngularLib.ViewModels.PublisherViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.MagazineViewModels
{
    public class UpdateMagazineViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfPublishing { get; set; }

        public virtual ICollection<ShortPublisherViewModel> Publisher { get; set; }

        public UpdateMagazineViewModel()
        {
            Publisher = new List<ShortPublisherViewModel>();
        }
    }
}
