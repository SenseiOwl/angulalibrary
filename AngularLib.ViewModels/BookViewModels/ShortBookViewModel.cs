﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.BookViewModels
{
    public class ShortBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
