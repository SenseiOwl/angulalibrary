﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.BookViewModels
{
    public class GetAllBookViewModel
    {
        public ICollection<GetBookViewModel> Books { get; set; }

        public GetAllBookViewModel()
        {
            Books = new List<GetBookViewModel>();
        }
    }
}
