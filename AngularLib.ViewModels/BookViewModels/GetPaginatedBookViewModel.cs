﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.BookViewModels
{
    public class GetPaginatedBookViewModel
    {
        public ICollection<GetBookViewModel> Books { get; set; }
        public PageInfo PageInfo { get; set; }
        
        public GetPaginatedBookViewModel()
        {
            Books = new List<GetBookViewModel>();
        }
    }
}
