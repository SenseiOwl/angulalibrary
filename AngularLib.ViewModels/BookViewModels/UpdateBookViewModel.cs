﻿using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.BookViewModels
{
    public class UpdateBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfPublishing { get; set; }
        public int YearOfWriting { get; set; }

        public ICollection<ShortPublisherViewModel> Publisher { get; set; }
        public ICollection<ShortAuthorViewModel> Authors { get; set; }

        public UpdateBookViewModel()
        {
            Publisher = new List<ShortPublisherViewModel>();
            Authors = new List<ShortAuthorViewModel>();
        }
    }
}
