﻿using AngularLib.ViewModels.PublisherViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.NewspaperViewModels
{
    public class UpdateNewspaperViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int YearOfPublishing { get; set; }

        public ICollection<ShortPublisherViewModel> Publisher { get; set; }

        public UpdateNewspaperViewModel()
        {
            Publisher = new List<ShortPublisherViewModel>();
        }
    }
}
