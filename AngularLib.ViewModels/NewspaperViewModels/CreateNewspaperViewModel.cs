﻿using AngularLib.ViewModels.PublisherViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.NewspaperViewModels
{
    public class CreateNewspaperViewModel
    {
        public string Name { get; set; }

        public int YearOfPublishing { get; set; }

        public ICollection<ShortPublisherViewModel> Publisher { get; set; }

        public CreateNewspaperViewModel()
        {
            Publisher = new List<ShortPublisherViewModel>();
        }
    }
}
