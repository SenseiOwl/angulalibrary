﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.NewspaperViewModels
{
    public class GetPaginatedNewspaperViewModel
    {
        public ICollection<GetNewspaperViewModel> Newspapers { get; set; }
        public PageInfo PageInfo { get; set; }

        public GetPaginatedNewspaperViewModel()
        {
            Newspapers = new List<GetNewspaperViewModel>();
        }
    }
}
