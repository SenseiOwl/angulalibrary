﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.NewspaperViewModels
{
    public class ShortNewspaperViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
