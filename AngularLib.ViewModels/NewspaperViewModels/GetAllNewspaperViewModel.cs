﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.NewspaperViewModels
{
    public class GetAllNewspaperViewModel
    {
        public ICollection<GetNewspaperViewModel> Newspapers { get; set;}

        public GetAllNewspaperViewModel()
        {
            Newspapers = new List<GetNewspaperViewModel>();
        }
    }
}
