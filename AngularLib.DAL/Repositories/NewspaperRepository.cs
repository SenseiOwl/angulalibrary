﻿using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AngularLib.DAL.Repositories
{
    public class NewspaperRepository : IRepository<Newspaper>
    {
        private string _connectionString = null;

        public NewspaperRepository(string conn)
        {
            _connectionString = conn;
        }

        public bool Create(Newspaper element)
        {
            #region SQLStrings
            var insertNewspaperSql = "INSERT INTO Newspapers(Name,Type,YearOfPublishing) VALUES (@Name,@Type,@YearOfPublishing);SELECT SCOPE_IDENTITY();";
            var insertPublisherNewsSql = "INSERT INTO PublisherNewspaper (PublisherId, NewspaperId) VALUES (@PublisherId, @NewspaperId)";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    var newspaperId = db.Query<int>(insertNewspaperSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing });

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherNewsSql, new { PublisherId = publisher.PublisherId, NewspaperId = newspaperId });
                    }

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }
            return true;

        }

        public bool Delete(int id)
        {
            #region SQLStrings
            var dltFrmPublisherNewsSql = "DELETE FROM PublisherNewspaper WHERE NewspaperId = @NewspaperId";
            var dltFrmNewspapersSql = "DELETE FROM Newspapers WHERE Id = @NewspaperId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(dltFrmPublisherNewsSql, new { NewspaperId = id });
                    db.Query(dltFrmNewspapersSql, new { NewspaperId = id });
                }
            }
            catch (Exception e)
            {
                //Log
                return false;
            }
            return true;

        }

        public Newspaper Get(int id)
        {
            Newspaper result;

            #region SQLStrings
            var newspaperSql = "SELECT * FROM Newspapers WHERE Id = @NewspaperId;";
            var publisherSql = "SELECT * FROM PublisherNewspaper b JOIN Publishers p ON p.Id = b.PublisherId WHERE b.NewspaperId = @NewspaperId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Newspaper>(newspaperSql, new { NewspaperId = id }).FirstOrDefault();
                    if (result == null)
                    {
                        return null;
                    }
                    var publishers = db.Query<PublisherNewspaper, Publisher, PublisherNewspaper>(publisherSql,
                        (publisherNewspaper, publisher) => { publisherNewspaper.Publisher = publisher; return publisherNewspaper; }, new { NewspaperId = id }, splitOn: "PublisherId,NewspaperId").ToList();

                    result.Publisher = publishers;
                }
            }
            catch (Exception e)
            {
                //Log
                return null;
            }

            return result;
        }

        public List<Newspaper> GetAll()
        {
            var result = new List<Newspaper>();

            #region SQLStrings
            var newspapersSql = "SELECT * FROM Newspapers";
            var publisherSql = "SELECT * FROM PublisherNewspaper b JOIN Publishers p ON p.Id = b.PublisherId JOIN Newspapers as newspaper on newspaper.Id = b.NewspaperId WHERE b.NewspaperId in @NewspaperId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Newspaper>(newspapersSql).ToList();
                    var publishers = db.Query<PublisherNewspaper, Publisher, Newspaper, PublisherNewspaper>(publisherSql,
                            (publisherNewspaper, publisher, newspaper) => { publisherNewspaper.Publisher = publisher; publisherNewspaper.Newspaper = newspaper; return publisherNewspaper; }, new { NewspaperId = result.Select(x => x.Id) }, splitOn: "PublisherId,NewspaperId,Id,Id").ToList();
                    foreach (var newspaper in result)
                    {
                        newspaper.Publisher = publishers.Where(x => x.NewspaperId == newspaper.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }

            return result;
        }


        public List<Newspaper> GetPaginatedNewspapers(int page, int size)
        {
            var result = new List<Newspaper>();

            #region SQLStrings
            var newspapersSql = "SELECT * FROM Newspapers ORDER BY Id OFFSET @Offset ROW FETCH NEXT @Items ROWS ONLY";
            var publisherSql = "SELECT * FROM PublisherNewspaper b JOIN Publishers p ON p.Id = b.PublisherId JOIN Newspapers as newspaper on newspaper.Id = b.NewspaperId WHERE b.NewspaperId in @NewspaperId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Newspaper>(newspapersSql, new { Offset = (page - 1) * size, Items = size }).ToList();
                    var publishers = db.Query<PublisherNewspaper, Publisher, Newspaper, PublisherNewspaper>(publisherSql,
                            (publisherNewspaper, publisher, newspaper) => { publisherNewspaper.Publisher = publisher; publisherNewspaper.Newspaper = newspaper; return publisherNewspaper; }, new { NewspaperId = result.Select(x => x.Id) }, splitOn: "PublisherId,NewspaperId,Id,Id").ToList();
                    foreach (var newspaper in result)
                    {
                        newspaper.Publisher = publishers.Where(x => x.NewspaperId == newspaper.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }

            return result;
        }

        public bool Update(Newspaper element)
        {
            #region SQLStrings
            var updateNewspaperSql = "UPDATE Newspapers SET Name = @Name,YearOfPublishing = @YearOfPublishing WHERE Newspapers.Id = @NewspaperId";
            var dltFrmPublisherNewsSql = "DELETE FROM PublisherNewspaper WHERE NewspaperId = @NewspaperId";
            var insertPublisherNewsSql = "INSERT INTO PublisherNewspaper (PublisherId, NewspaperId) VALUES (@PublisherId, @NewspaperId)";
            
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(updateNewspaperSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing, NewspaperId = element.Id });

                    db.Query(dltFrmPublisherNewsSql, new { NewspaperId = element.Id });

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherNewsSql, new { PublisherId = publisher.PublisherId, NewspaperId = element.Id });
                    }

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;

        }

        public int GetNewspapersCount()
        {
            int result;
            #region SQLStrings
            var countNewspapersSql = "SELECT COUNT(*) FROM Newspapers";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<int>(countNewspapersSql).FirstOrDefault();
                };
            }
            catch
            {
                return 0;
            }


            return result;
        }
    }
}
