﻿using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AngularLib.DAL.Repositories
{
    public class BookRepository : IRepository<Book>
    {
        private string _connectionString = null;

        public BookRepository(string conn)
        {
            _connectionString = conn;
        }

        public bool Create(Book element)
        {
            #region SQLStrings
            var insertBooksSql = "INSERT INTO Books (Name,Type,YearOfPublishing,YearOfWriting) VALUES (@Name,@Type,@YearOfPublishing,@YearOfWriting);SELECT SCOPE_IDENTITY();";
            var insertAuthorBookSql = "INSERT INTO AuthorBook (AuthorId, BookId) VALUES (@AuthorId,@BookId)";
            var insertPublisherBookSql = "INSERT INTO PublisherBook (PublisherId, BookId) VALUES (@PublisherId, @BookId)";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                   var bookId= db.Query<int>(insertBooksSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing, YearOfWriting = element.YearOfWriting });

                    foreach (var author in element.Authors)
                    {
                        db.Query(insertAuthorBookSql, new { AuthorId = author.AuthorId, BookId = bookId });
                    }

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherBookSql, new { PublisherId = publisher.PublisherId, BookId = bookId });
                    }

                }
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;

        }

        public bool Delete(int id)
        {
            #region SQLStrings
            var dltFrmAuthorBookSql = "DELETE FROM AuthorBook WHERE BookId = @BookId";
            var dltFrmPublisherBookSql = "DELETE FROM PublisherBook WHERE BookId = @BookId";
            var dltFrmBooksSql = "DELETE FROM Books WHERE Id = @BookId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(dltFrmAuthorBookSql, new { BookId = id });
                    db.Query(dltFrmPublisherBookSql, new { BookId = id });
                    db.Query(dltFrmBooksSql, new { BookId = id });
                }
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;
        }

        public Book Get(int id)
        {
            Book result;

            #region SQLStrings
            var booksSql = "SELECT * FROM Books WHERE Id = @BookId;";
            var authorsSql = "SELECT * FROM AuthorBook b JOIN Authors a ON a.Id = b.AuthorId WHERE b.BookId = @BookId";
            var publisherSql = "SELECT * FROM PublisherBook b JOIN Publishers p ON p.Id = b.PublisherId WHERE b.BookId = @BookId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Book>(booksSql, new { BookId = id }).FirstOrDefault();
                    if (result == null)
                    {
                        return null;
                    }
                    var authors = db.Query<AuthorBook, Author, AuthorBook>(authorsSql,
                        (authorBook, author) => { authorBook.Author = author; return authorBook; }, new { BookId = id }, splitOn: "AuthorId,BookId").ToList();
                    var publishers = db.Query<PublisherBook, Publisher, PublisherBook>(publisherSql,
                        (publisherBook, publisher) => { publisherBook.Publisher = publisher; return publisherBook; }, new { BookId = id }, splitOn: "PublisherId,BookId").ToList();

                    result.Authors = authors;
                    result.Publisher = publishers;

                }

                return result;
            }
            catch (Exception e)
            {
                //Log
                return null;
            }

        }

        public List<Book> GetAll()
        {
            var result = new List<Book>();

            #region SQLStrings
            var booksSql = "SELECT * FROM Books";
            var authorsSql = "SELECT * FROM AuthorBook b JOIN Authors a ON a.Id = b.AuthorId join Books as book on book.Id= b.BookId WHERE b.BookId in @BookId";
            var publisherSql = "SELECT * FROM PublisherBook b JOIN Publishers p ON p.Id = b.PublisherId join Books as book on book.Id= b.BookId WHERE b.BookId in @BookId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Book>(booksSql).ToList();
                    var authors = db.Query<AuthorBook, Author, Book, AuthorBook>(authorsSql,
                        (authorBook, author, book) => { authorBook.Author = author;authorBook.Book = book; return authorBook; }, new { BookId = result.Select(x=>x.Id) }, splitOn: "AuthorId,BookId,Id,Id").ToList();
                    var publishers = db.Query<PublisherBook, Publisher,Book, PublisherBook>(publisherSql,
                            (publisherBook, publisher, book) => { publisherBook.Publisher = publisher; publisherBook.Book = book; return publisherBook; }, new { BookId = result.Select(x => x.Id) }, splitOn: "PublisherId,BookId,Id,Id").ToList();

                    foreach (var book in result)
                    {                      
                        
                        book.Authors = authors.Where(x=>x.BookId==book.Id).ToList();
                        book.Publisher = publishers.Where(x => x.BookId == book.Id).ToList();
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                //Log
                return result;
            }


        }

        public bool Update(Book element)
        {

            #region SQLStrings
            var updateBooksSql = "UPDATE Books SET Name = @Name,YearOfPublishing = @YearOfPublishing ,YearOfWriting = @YearOfWriting WHERE Books.Id = @BookId";
            var dltFrmAuthorBookSql = "DELETE FROM AuthorBook WHERE BookId = @BookId";
            var dltFrmPublisherBookSql = "DELETE FROM PublisherBook WHERE BookId = @BookId";
            var insertAuthorBookSql = "INSERT INTO AuthorBook (AuthorId, BookId) VALUES (@AuthorId,@BookId)";
            var insertPublisherBookSql = "INSERT INTO PublisherBook (PublisherId, BookId) VALUES (@PublisherId, @BookId)";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(updateBooksSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing, YearOfWriting = element.YearOfWriting, BookId = element.Id });

                    db.Query(dltFrmAuthorBookSql, new { BookId = element.Id });
                    db.Query(dltFrmPublisherBookSql, new { BookId = element.Id });

                    foreach (var author in element.Authors)
                    {
                        db.Query(insertAuthorBookSql, new { AuthorId = author.AuthorId, BookId = element.Id });
                    }

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherBookSql, new { PublisherId = publisher.PublisherId, BookId = element.Id });
                    }

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;

        }


        public List<Book> GetPaginatedBook(int page, int size)
        {
            var result = new List<Book>();

            #region SQLStrings
            var booksSql = "SELECT * FROM Books ORDER BY Id OFFSET @Offset ROW FETCH NEXT @Items ROWS ONLY";
            var authorsSql = "SELECT * FROM AuthorBook b JOIN Authors a ON a.Id = b.AuthorId join Books as book on book.Id= b.BookId WHERE b.BookId in @BookId";
            var publisherSql = "SELECT * FROM PublisherBook b JOIN Publishers p ON p.Id = b.PublisherId join Books as book on book.Id= b.BookId WHERE b.BookId in @BookId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Book>(booksSql, new { Offset = (page - 1) * size, Items = size }).ToList();
                    var authors = db.Query<AuthorBook, Author, Book, AuthorBook>(authorsSql,
                        (authorBook, author, book) => { authorBook.Author = author; authorBook.Book = book; return authorBook; }, new { BookId = result.Select(x => x.Id) }, splitOn: "AuthorId,BookId,Id,Id").ToList();
                    var publishers = db.Query<PublisherBook, Publisher, Book, PublisherBook>(publisherSql,
                            (publisherBook, publisher, book) => { publisherBook.Publisher = publisher; publisherBook.Book = book; return publisherBook; }, new { BookId = result.Select(x => x.Id) }, splitOn: "PublisherId,BookId,Id,Id").ToList();
                    foreach (var book in result)
                    {
                        book.Authors = authors.Where(x => x.BookId == book.Id).ToList();
                        book.Publisher = publishers.Where(x => x.BookId == book.Id).ToList();
                    }
                }

                return result;
            }
            catch(Exception e)
            {
                //Log
                return result;
            }


        }

        public int GetBooksCount()
        {
            int result;
            #region SQLStrings
            var countBooksSql = "SELECT COUNT(*) FROM Books";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<int>(countBooksSql).FirstOrDefault();
                };
            }
            catch
            {
                return 0;
            }


            return result;
        }
    }

}

