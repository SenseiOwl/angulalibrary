﻿using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AngularLib.DAL.Repositories
{
    public class PublisherRepository : IRepository<Publisher>
    {
        private string _connectionString = null;

        public PublisherRepository(string conn)
        {
            _connectionString = conn;
        }

        public bool Create(Publisher element)
        {
            #region SQLStrings
            var insertPublishersSql = "INSERT INTO Publishers (Name, YearOfOpening) VALUES (@Name, @YearOfOpening)";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(insertPublishersSql,
                        new { Name = element.Name, YearOfOpening = element.YearOfOpening });
                }
            }
            catch(Exception e)
            {
                //Log
                return false;
            }

            return true;


        }

        public bool Delete(int id)
        {
            #region SQLStrings
            var numberDependentsSql = "SELECT SUM(T.c) FROM (SELECT COUNT(*) AS c FROM PublisherBook p WHERE p.PublisherId = @PublisherId UNION ALL SELECT COUNT(*) AS c FROM PublisherNewspaper p WHERE p.PublisherId = @PublisherId UNION ALL SELECT COUNT(*) AS c FROM PublisherMagazine p WHERE p.PublisherId = @PublisherId) T";
            var dltPublisherSql = "DELETE FROM Publishers WHERE Id = @PublisherId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    var dependenceCount = 0;
                    dependenceCount += db.Query<int>(numberDependentsSql,
                        new { PublisherId = id }).FirstOrDefault();
                    if (dependenceCount > 0)
                    {
                        return false;
                    }
                    db.Query(dltPublisherSql, new { PublisherId = id });

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;


        }

        public Publisher Get(int id)
        {
            Publisher result;

            #region SQLStrings
            var publisherSql = "SELECT * FROM Publishers WHERE Id = @PublisherId;";
            var booksSql = "SELECT * FROM PublisherBook b JOIN Books p ON p.Id = b.BookId WHERE b.PublisherId = @PublisherId";
            var magazinesSql = "SELECT * FROM PublisherMagazine b JOIN Magazines p ON p.Id = b.MagazineId WHERE b.PublisherId = @PublisherId";
            var newspapersSql = "SELECT * FROM PublisherNewspaper b JOIN Newspapers p ON p.Id = b.NewspaperId WHERE b.PublisherId = @PublisherId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Publisher>(publisherSql, new { PublisherId = id }).FirstOrDefault();
                    if (result == null)
                    {
                        return null;
                    }
                    var books = db.Query<PublisherBook, Book, PublisherBook>(booksSql, (publisherBook, book) =>
                    { publisherBook.Book = book; return publisherBook; }, new { PublisherId = id }, splitOn: "PublisherId,BookId").ToList();
                    var newspapers = db.Query<PublisherNewspaper, Newspaper, PublisherNewspaper>(newspapersSql, (publisherNews, newspaper) =>
                    { publisherNews.Newspaper = newspaper; return publisherNews; }, new { PublisherId = id }, splitOn: "PublisherId,NewspaperId").ToList();
                    var magazines = db.Query<PublisherMagazine, Magazine, PublisherMagazine>(magazinesSql, (publisherMag, magazine) =>
                    { publisherMag.Magazine = magazine; return publisherMag; }, new { PublisherId = id }, splitOn: "PublisherId,MagazineId").ToList();

                    result.Books = books;
                    result.Magazines = magazines;
                    result.Newspapers = newspapers;
                }
            }
            catch (Exception e)
            {
                //Log
                return null;
            }


            return result;
        }

        public List<Publisher> GetAll()
        {

            var result = new List<Publisher>();

            #region SQLStrings
            var publisherSql = "SELECT * FROM Publishers";
            var booksSql = "SELECT * FROM PublisherBook b JOIN Publishers p ON p.Id = b.PublisherId join Books as book on book.Id= b.BookId WHERE b.PublisherId in @PublisherId";
            var magazinesSql = "SELECT * FROM PublisherMagazine b JOIN Publishers p ON p.Id = b.PublisherId join Magazines as magazine on magazine.Id= b.MagazineId WHERE b.PublisherId in @PublisherId";
            var newspapersSql = "SELECT * FROM PublisherNewspaper b JOIN Publishers p ON p.Id = b.PublisherId JOIN Newspapers as newspaper on newspaper.Id = b.NewspaperId WHERE b.PublisherId in @PublisherId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Publisher>(publisherSql).ToList();
                    var books = db.Query<PublisherBook, Book, Publisher, PublisherBook>(booksSql, (publisherBook, book, publisher) =>
                    { publisherBook.Book = book; publisherBook.Publisher = publisher; return publisherBook; }, new { PublisherId = result.Select(x => x.Id) },
                                        splitOn: "BookId,PublisherId,Id,Id").ToList();
                    var newspapers = db.Query<PublisherNewspaper, Newspaper, Publisher, PublisherNewspaper>(newspapersSql, (publisherNews, newspaper, publisher) =>
                     { publisherNews.Newspaper = newspaper; publisherNews.Publisher = publisher; return publisherNews; }, new { PublisherId = result.Select(x => x.Id) }, splitOn: "NewspaperId,PublisherId,Id,Id").ToList();
                    var magazines = db.Query<PublisherMagazine, Magazine, Publisher, PublisherMagazine>(magazinesSql, (publisherMag, magazine, publisher) =>
                    { publisherMag.Magazine = magazine; publisherMag.Publisher = publisher; return publisherMag; }, new { PublisherId = result.Select(x => x.Id) },
                                    splitOn: "MagazineId,PublisherId,Id,Id").ToList();
                    foreach (var publisher in result)
                    {
                        publisher.Books = books.Where(x => x.PublisherId == publisher.Id).ToList();
                        publisher.Magazines = magazines.Where(x => x.PublisherId == publisher.Id).ToList();
                        publisher.Newspapers = newspapers.Where(x => x.PublisherId == publisher.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }


            return result;
        }

        public List<Publisher> GetPaginatedPublisher(int page, int size)
        {
            var result = new List<Publisher>();

            #region SQLStrings
            var publisherSql = "SELECT * FROM Publishers ORDER BY Id OFFSET @Offset ROW FETCH NEXT @Items ROWS ONLY";
            var booksSql = "SELECT * FROM PublisherBook b JOIN Publishers p ON p.Id = b.PublisherId join Books as book on book.Id= b.BookId WHERE b.PublisherId in @PublisherId";
            var magazinesSql = "SELECT * FROM PublisherMagazine b JOIN Publishers p ON p.Id = b.PublisherId join Magazines as magazine on magazine.Id= b.MagazineId WHERE b.PublisherId in @PublisherId";
            var newspapersSql = "SELECT * FROM PublisherNewspaper b JOIN Publishers p ON p.Id = b.PublisherId JOIN Newspapers as newspaper on newspaper.Id = b.NewspaperId WHERE b.PublisherId in @PublisherId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Publisher>(publisherSql, new { Offset = (page - 1) * size, Items = size }).ToList();
                    var books = db.Query<PublisherBook,  Publisher, Book, PublisherBook>(booksSql, (publisherBook, publisher, book) =>
                    { publisherBook.Book = book; publisherBook.Publisher = publisher; return publisherBook; }, new { PublisherId = result.Select(x => x.Id) },
                                        splitOn: "BookId,PublisherId,Id,Id").ToList();
                    var newspapers = db.Query<PublisherNewspaper,  Publisher, Newspaper, PublisherNewspaper>(newspapersSql, (publisherNews, publisher, newspaper) =>
                    { publisherNews.Newspaper = newspaper; publisherNews.Publisher = publisher; return publisherNews; }, new { PublisherId = result.Select(x => x.Id) }, splitOn: "NewspaperId,PublisherId,Id,Id").ToList();
                    var magazines = db.Query<PublisherMagazine,  Publisher, Magazine, PublisherMagazine>(magazinesSql, (publisherMag, publisher, magazine) =>
                    { publisherMag.Magazine = magazine; publisherMag.Publisher = publisher; return publisherMag; }, new { PublisherId = result.Select(x => x.Id) },
                                    splitOn: "MagazineId,PublisherId,Id,Id").ToList();
                    foreach (var publisher in result)
                    {
                        publisher.Books = books.Where(x => x.PublisherId == publisher.Id).ToList();
                        publisher.Magazines = magazines.Where(x => x.PublisherId == publisher.Id).ToList();
                        publisher.Newspapers = newspapers.Where(x => x.PublisherId == publisher.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }


            return result;
        }

        public bool Update(Publisher element)
        {
            #region SQLStrings
            var updPublisherSql = "UPDATE Publishers SET Name = @Name, YearOfOpening = @YearOfOpening WHERE Publishers.Id = @PublishersId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(updPublisherSql,
                        new { Name = element.Name, YearOfOpening = element.YearOfOpening, PublishersId = element.Id });
                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;


        }


        public int GetPublishersCount()
        {
            int result;
            #region SQLStrings
            var countPublishersSql = "SELECT COUNT(*) FROM Publishers";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<int>(countPublishersSql).FirstOrDefault();
                };
            }
            catch
            {
                return 0;
            }


            return result;
        }
    }
}
