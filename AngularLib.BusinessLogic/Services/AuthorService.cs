﻿using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.BookViewModels;
using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AngularLib.BusinessLogic.Common;
using AngularLib.ViewModels;

namespace AngularLib.BusinessLogic.Services
{
    public class AuthorService
    {
        private AuthorRepository _repository;

        public AuthorService(string conn)
        {
            _repository = new AuthorRepository(conn);
        }

        public async Task<GetAllAuthorViewModel> GetAll()
        {
            List<Author> authors = await _repository.GetAll();
            var result = new GetAllAuthorViewModel();
            result.Authors = Mapper.Map<ICollection<Author>, ICollection<GetAuthorViewModel>>(authors);
            return result;
        }

        public async Task<GetPaginatedAuthorViewModel> GetPaginated(int page, int size)
        {
            List<Author> authors = await _repository.GetPaginatedAuthors(page,size);
            var result = new GetPaginatedAuthorViewModel();
            result.Authors = Mapper.Map<ICollection<Author>, ICollection<GetAuthorViewModel>>(authors);
            var pageInfo = new PageInfo();
            pageInfo.CurrentPage = page;
            pageInfo.ItemsPerPage = size;
            pageInfo.TotalItems = await _repository.GetAuthorsCount();
            result.PageInfo = pageInfo;
            return result;
        }

        public async Task<GetAuthorViewModel> Get(int id)
        {
            Author author = await _repository.Get(id);
            if (author == null)
            {
                throw new BusinessLogicException("Can't find author!");
            }
            var result = Mapper.Map<Author, GetAuthorViewModel>(author);
            return result;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<bool> Update(UpdateAuthorViewModel model)
        {
            Author author = await _repository.Get(model.Id);
            Mapper.Map(model,author);
            return await _repository.Update(author);
        }

        public async Task<bool> Create(CreateAuthorViewModel model)
        {
            var author = new Author();
            Mapper.Map(model, author);
            return await _repository.Create(author);

        }

        public async Task<IEnumerable<ShortAuthorViewModel>> GetShortAuthors()
        {
            List<Author> authors = await _repository.GetAll();
            var result = Mapper.Map<ICollection<Author>, ICollection<ShortAuthorViewModel>>(authors);
            return result;
        }
    }
}
