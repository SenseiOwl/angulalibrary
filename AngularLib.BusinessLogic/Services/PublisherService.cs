﻿
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using AngularLib.BusinessLogic.Common;
using AngularLib.ViewModels;

namespace AngularLib.BusinessLogic.Services
{
    public class PublisherService
    {
        private PublisherRepository _repository;

        public PublisherService(string conn)
        {
            _repository = new PublisherRepository(conn);
        }

        public GetAllPublisherViewModel GetAll()
        {
            List<Publisher> publishers = _repository.GetAll();
            var result = new GetAllPublisherViewModel();
            result.Publishers = Mapper.Map<ICollection<Publisher>, ICollection<GetPublisherViewModel>>(publishers);
            return result;
        }

        public GetPaginatedPublisherViewModel GetPaginated(int page, int size)
        {
            List<Publisher> publishers = _repository.GetPaginatedPublisher(page, size);
            var result = new GetPaginatedPublisherViewModel();
            result.Publishers = Mapper.Map<ICollection<Publisher>, ICollection<GetPublisherViewModel>>(publishers);
            var pageInfo = new PageInfo();
            pageInfo.CurrentPage = page;
            pageInfo.ItemsPerPage = size;
            pageInfo.TotalItems = _repository.GetPublishersCount();
            result.PageInfo = pageInfo;
            return result;
        }

        public GetPublisherViewModel Get(int id)
        {
            Publisher publisher = _repository.Get(id);

            if (publisher == null)
            {
                throw new BusinessLogicException("Can't find Publisher!");
            }

            var result = Mapper.Map<Publisher, GetPublisherViewModel>(publisher);
            return result;
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public bool Update(UpdatePublisherViewModel model)
        {
            Publisher publisher = _repository.Get(model.Id);
            Mapper.Map(model, publisher);
            return _repository.Update(publisher);
        }

        public bool Create(CreatePublisherViewModel model)
        {
            var publisher = new Publisher();
            Mapper.Map(model, publisher);
            return _repository.Create(publisher);
        }

        public IEnumerable<ShortPublisherViewModel> GetShortPublishers()
        {
            List<Publisher> publishers = _repository.GetAll();
            var result = Mapper.Map<ICollection<Publisher>, ICollection<ShortPublisherViewModel>>(publishers);
            return result;
        }
    }
}
