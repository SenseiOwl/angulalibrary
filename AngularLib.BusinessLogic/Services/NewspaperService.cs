﻿using AngularLib.ViewModels;
using AngularLib.ViewModels.NewspaperViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using AngularLib.BusinessLogic.Common;

namespace AngularLib.BusinessLogic.Services
{
    public class NewspaperService
    {
        private NewspaperRepository _repository;

        public NewspaperService(string conn)
        {
            _repository = new NewspaperRepository(conn);
        }

        public GetAllNewspaperViewModel GetAll()
        {
            List<Newspaper> newspapers = _repository.GetAll();
            var result = new GetAllNewspaperViewModel();
            result.Newspapers = Mapper.Map<ICollection<Newspaper>, ICollection<GetNewspaperViewModel>>(newspapers);
            return result;
        }

        public GetPaginatedNewspaperViewModel GetPaginated(int page, int size)
        {
            List<Newspaper> newspapers = _repository.GetPaginatedNewspapers(page, size);
            var result = new GetPaginatedNewspaperViewModel();
            result.Newspapers = Mapper.Map<ICollection<Newspaper>, ICollection<GetNewspaperViewModel>>(newspapers);
            var pageInfo = new PageInfo();
            pageInfo.CurrentPage = page;
            pageInfo.ItemsPerPage = size;
            pageInfo.TotalItems = _repository.GetNewspapersCount();
            result.PageInfo = pageInfo;
            return result;
        }

        public GetNewspaperViewModel Get(int id)
        {
            Newspaper newspaper = _repository.Get(id);
            if (newspaper == null)
            {
                throw new BusinessLogicException("Can't find Newspaper!");
            }
            var result = Mapper.Map<Newspaper, GetNewspaperViewModel>(newspaper);

            return result;
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public bool Update(UpdateNewspaperViewModel model)
        {
            Newspaper newspaper = _repository.Get(model.Id);
            Mapper.Map(model, newspaper);
            return _repository.Update(newspaper);
        }

        public bool Create(CreateNewspaperViewModel model)
        {
            Newspaper newspaper = new Newspaper();
            Mapper.Map(model, newspaper);
            return _repository.Create(newspaper);
        }
    }
}
