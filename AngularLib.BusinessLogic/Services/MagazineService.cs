﻿using AngularLib.ViewModels;
using AngularLib.ViewModels.MagazineViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using AngularLib.BusinessLogic.Common;

namespace AngularLib.BusinessLogic.Services
{
    public class MagazineService
    {
        private MagazinesRepository _repository;

        public MagazineService(string conn)
        {
            _repository = new MagazinesRepository(conn);
        }

        public GetAllMagazineViewModel GetAll()
        {
            List<Magazine> magazines = _repository.GetAll();
            var result = new GetAllMagazineViewModel();
            result.Magazines = Mapper.Map<ICollection<Magazine>, ICollection<GetMagazineViewModel>>(magazines);
            return result;
        }

        public GetPaginatedMagazineViewModel GetPaginated(int page, int size)
        {
            List<Magazine> magazines = _repository.GetPaginatedMagazines(page, size);
            var result = new GetPaginatedMagazineViewModel();
            result.Magazines = Mapper.Map<ICollection<Magazine>, ICollection<GetMagazineViewModel>>(magazines);
            var pageInfo = new PageInfo();
            pageInfo.CurrentPage = page;
            pageInfo.ItemsPerPage = size;
            pageInfo.TotalItems = _repository.GetMagazinesCount();
            result.PageInfo = pageInfo;
            return result;
        }

        public GetMagazineViewModel Get(int id)
        {
            Magazine magazine = _repository.Get(id);
            if (magazine == null)
            {
                throw new BusinessLogicException("Can't find Magazine!");
            }
            var result = Mapper.Map<Magazine, GetMagazineViewModel>(magazine);
            return result;
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public bool Update(UpdateMagazineViewModel model)
        {
            Magazine magazine = _repository.Get(model.Id);
            Mapper.Map(model, magazine);
            return _repository.Update(magazine);
        }

        public bool Create(CreateMagazineViewModel model)
        {
            Magazine magazine = new Magazine();
            Mapper.Map(model, magazine);
            return _repository.Create(magazine);
        }
    }
}
