﻿using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.BookViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class BookProfiles : Profile
    {
        public BookProfiles()
        {
            CreateMap<Book, ShortBookViewModel>();

            CreateMap<Book, GetBookViewModel>()
                .ForMember(dest => dest.Authors,
                    opt => opt.ResolveUsing(x => Mapper.Map<ICollection<AuthorBook>, ICollection<ShortAuthorViewModel>>(x.Authors)))
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherBook>, ICollection<ShortPublisherViewModel>>(x.Publisher)));
            CreateMap<GetBookViewModel, Book>()
                .ForMember(dest => dest.Authors,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<AuthorBook>();
                        foreach (var author in x.Authors)
                        {
                            var authorBook = new AuthorBook();
                            authorBook.AuthorId = author.Id;
                            authorBook.BookId = x.Id;
                            result.Add(authorBook);
                        }
                        return result;
                    }))
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherBook>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherBook = new PublisherBook();
                            publisherBook.PublisherId = publisher.Id;
                            publisherBook.BookId = x.Id;
                            result.Add(publisherBook);
                        }
                        return result;
                    }));

            CreateMap<CreateBookViewModel, Book>()
                .ForMember(dest => dest.Authors,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<AuthorBook>();
                        foreach (var author in x.Authors)
                        {
                            var authorBook = new AuthorBook();
                            authorBook.AuthorId = author.Id;
                            result.Add(authorBook);
                        }
                        return result;
                    }))
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherBook>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherBook = new PublisherBook();
                            publisherBook.PublisherId = publisher.Id;
                            result.Add(publisherBook);
                        }
                        return result;
                    }));

            CreateMap<UpdateBookViewModel, Book>()
                .ForMember(dest => dest.Authors,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<AuthorBook>();
                        foreach (var author in x.Authors)
                        {
                            var authorBook = new AuthorBook();
                            authorBook.AuthorId = author.Id;
                            authorBook.BookId = x.Id;
                            result.Add(authorBook);
                        }
                        return result;
                    }))
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherBook>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherBook = new PublisherBook();
                            publisherBook.PublisherId = publisher.Id;
                            publisherBook.BookId = x.Id;
                            result.Add(publisherBook);
                        }
                        return result;
                    }));


        }
    }
}
