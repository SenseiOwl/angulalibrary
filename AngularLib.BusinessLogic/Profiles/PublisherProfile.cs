﻿using AngularLib.ViewModels.BookViewModels;
using AngularLib.ViewModels.MagazineViewModels;
using AngularLib.ViewModels.NewspaperViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class PublisherProfile : Profile
    {
        public PublisherProfile()
        {
            CreateMap<Publisher, ShortPublisherViewModel>();

            CreateMap<Publisher, GetPublisherViewModel>()
                .ForMember(dest => dest.Books, opt => opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherBook>, ICollection<ShortBookViewModel>>(x.Books)))
                .ForMember(dest => dest.Magazines, opt =>
                    opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherMagazine>, ICollection<ShortMagazineViewModel>>(x.Magazines)))
                .ForMember(dest => dest.Newspapers,
                    opt => opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherNewspaper>, ICollection<ShortNewspaperViewModel>>(x.Newspapers)));
            CreateMap<GetPublisherViewModel, Publisher>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Books, opt => opt.Ignore())
                .ForMember(dest => dest.Newspapers, opt => opt.Ignore())
                .ForMember(dest => dest.Magazines, opt => opt.Ignore());

            CreateMap<CreatePublisherViewModel, Publisher>();
            CreateMap<UpdatePublisherViewModel, Publisher>();
        }
    }
}
