﻿using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.BookViewModels;
using AngularLib.ViewModels.MagazineViewModels;
using AngularLib.ViewModels.NewspaperViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class SharedProfile : Profile
    {
        public SharedProfile()
        {
            CreateMap<AuthorBook, ShortBookViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Book.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Book.Name));
            CreateMap<AuthorBook, ShortAuthorViewModel>()
               .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Author.Id))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Author.Name));

            CreateMap<PublisherBook, ShortBookViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Book.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Book.Name));
            CreateMap<PublisherBook, ShortPublisherViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Publisher.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Publisher.Name));

            CreateMap<PublisherMagazine, ShortMagazineViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Magazine.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Magazine.Name));
            CreateMap<PublisherMagazine, ShortPublisherViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Publisher.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Publisher.Name));

            CreateMap<PublisherNewspaper, ShortNewspaperViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Newspaper.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Newspaper.Name));
            CreateMap<PublisherNewspaper, ShortPublisherViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Publisher.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.Publisher.Name));
        }
    }
}
