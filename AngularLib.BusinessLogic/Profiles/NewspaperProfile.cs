﻿using AngularLib.ViewModels.NewspaperViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class NewspaperProfile : Profile
    {
        public NewspaperProfile()
        {
            CreateMap<Newspaper, ShortNewspaperViewModel>();

            CreateMap<Newspaper, GetNewspaperViewModel>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherNewspaper>, ICollection<ShortPublisherViewModel>>(x.Publisher)));
            CreateMap<GetNewspaperViewModel, Newspaper>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherNewspaper>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherNewspaper = new PublisherNewspaper();
                            publisherNewspaper.PublisherId = publisher.Id;
                            publisherNewspaper.NewspaperId = x.Id;
                            result.Add(publisherNewspaper);
                        }
                        return result;
                    }));

            CreateMap<CreateNewspaperViewModel, Newspaper>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherNewspaper>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherNewspaper = new PublisherNewspaper();
                            publisherNewspaper.PublisherId = publisher.Id;
                            result.Add(publisherNewspaper);
                        }
                        return result;
                    }));

            CreateMap<UpdateNewspaperViewModel, Newspaper>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherNewspaper>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherNewspaper = new PublisherNewspaper();
                            publisherNewspaper.PublisherId = publisher.Id;
                            publisherNewspaper.NewspaperId = x.Id;
                            result.Add(publisherNewspaper);
                        }
                        return result;
                    }));
        }
    }
}
