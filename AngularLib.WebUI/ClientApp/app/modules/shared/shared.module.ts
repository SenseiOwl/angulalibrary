﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationComponent } from './pagination/pagination.component';
import { AuthorService } from '../../services/author.service';
import { BookService } from '../../services/book.service';
import { ConfigService } from '../../services/config.service';
import { CookieService } from '../../services/cookie.service';
import { MagazineService } from '../../services/magazine.service';
import { NewspaperService } from '../../services/newspaper.service';
import { PublisherService } from '../../services/publisher.service';
import { UserService } from '../../services/user.service';
import { AuthGuard } from '../../common/auth.guard';





@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        PaginationComponent
    ],
    exports: [
        PaginationComponent,
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AuthorService,
                BookService,
                ConfigService,
                CookieService,
                MagazineService,
                NewspaperService,
                PublisherService,
                UserService,
                AuthGuard
            ]
        }
    }
}