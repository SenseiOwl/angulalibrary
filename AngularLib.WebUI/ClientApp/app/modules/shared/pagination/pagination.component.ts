﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PageInfo } from '../../../viewModels/pageInfo';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {
    @Input() pageInfo: PageInfo;

    @Output() goPrev = new EventEmitter<boolean>();
    @Output() goNext = new EventEmitter<boolean>();
    @Output() goChangePerPage = new EventEmitter<number>();

    constructor() {

    }

    onPrev(): void {
        this.goPrev.emit(true);
    }

    onNext(): void {
        if (this.pageInfo.currentPage == this.pageInfo.totalPages) {
            return;
        }
        this.goNext.emit(true);
    }

    tryPrevPage(): number {
        if (this.pageInfo.currentPage < 2) {
            return 0;
        }
        return this.pageInfo.currentPage - 1;
    }

    tryNextPage(): number {
        if (this.pageInfo.currentPage == this.pageInfo.totalPages) {
            return 0;
        }
        return this.pageInfo.currentPage + 1;
    }

    onChange(perPage: number) {
        this.goChangePerPage.emit(perPage);
    }
}