﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../../../services/user.service";
import { UserRegistration } from "../../../viewModels/authViewModels/user.registration";

@Component({
    selector: 'app-registration-form',
    templateUrl: './registration-form.component.html',
    styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

    errors: string;
    isRequesting: boolean;
    submitted: boolean = false;

    constructor(private _userService: UserService, private _router: Router) { }

    ngOnInit() {
    }

    registerUser({ value, valid }: { value: UserRegistration, valid: boolean }) {
        this.submitted = true;
        this.isRequesting = true;
        this.errors = '';
        if (valid) {
            this._userService.register(value.email, value.password, value.firstName, value.lastName, value.location)
                .finally(() => this.isRequesting = false)
                .subscribe(
                result => {
                    if (result) {
                        this._router.navigate(['/login'], { queryParams: { brandNew: true, email: value.email } });
                    }
                },
                errors => this.errors = errors);
        }
    }



}