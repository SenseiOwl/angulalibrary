import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { PaginatedBook } from '../../../viewModels/bookViewModels/paginatedBookView';
import { BookService } from '../../../services/book.service';

@Component({
    selector: 'ad-books',
    templateUrl: './books.component.html'
})
export class AdBooksComponent {
    public books: PaginatedBook;
    message: string;
    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _bookService: BookService, private _acRouter: ActivatedRoute) {
        this.getBooks(this.CurrentPage);
        if (this._acRouter.snapshot.params["message"]) {
            this.message = this._acRouter.snapshot.params["message"];
        }
    }

    getBooks(page: number) {

        this._bookService.getPaginatedBooks(page, this.itemPerPage).subscribe(
            result => {
                this.books = result;
            }, error => console.error(error));
    }

    delete(ID: any) {
        var ans = confirm("Do you want to delete the book?");
        if (ans) {
            this._bookService.deleteBook(ID).subscribe((value) => {
                if ((value as boolean) == false) {
                    this.getBooks(this.CurrentPage);
                    this.message = "The book was not deleted";

                }
                else {
                    this.getBooks(this.CurrentPage);
                    this.message = "The book was deleted";
                }
            }, error => console.error(error))
        }
    }

    onMessageClick() {
        this.message = "";
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getBooks(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getBooks(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}


