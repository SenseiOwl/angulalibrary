import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedMagazine } from '../../../viewModels/magazineViewModels/paginatedMagazineView';
import { MagazineService } from '../../../services/magazine.service';

@Component({
    selector: 'ad-magazines',
    templateUrl: './magazines.component.html'
})
export class AdMagazinesComponent {
    public magazines: PaginatedMagazine;

    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _bookService: MagazineService) {
        this.getMagazines(this.CurrentPage);
    }

    getMagazines(page: number) {
        this._bookService.getPaginatedMagazines(page, this.itemPerPage).subscribe(result => {
            this.magazines = result;
        }, error => console.error(error));
    }

    delete(ID: any) {
        var ans = confirm("Do you want to delete customer with Id: " + ID);
        if (ans) {
            this._bookService.deleteMagazine(ID).subscribe((data) => {
                this.getMagazines(this.CurrentPage);
            }, error => console.error(error))
        }
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getMagazines(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getMagazines(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
    
}



