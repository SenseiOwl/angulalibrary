﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AdBooksComponent } from './books/books.component';
import { AdNewspapersComponent } from './newspapers/newspapers.component';
import { AdMagazinesComponent } from './magazines/magazines.component';
import { AdPublishersComponent } from './publishers/publishers.component';
import { AdPublisherChangerComponent } from './publisherchanger/publisherchanger.component';
import { AdAuthorsComponent } from './authros/authors.component';
import { AdAuthorChangerComponent } from './authorchanger/authorchanger.component';
import { AdBookChangerComponent } from './bookchanger/bookchanger.component';
import { AdMagazineChangerComponent } from './magazinechanger/magazinechanger.component';
import { AdNewspaperChangerComponent } from './newspaperchanger/newspaperchanger.component';
import { AuthGuard } from '../../common/auth.guard';

export const routing: ModuleWithProviders = RouterModule.forChild([
    {
        path: 'admin', component: AdminComponent, canActivate: [AuthGuard],
        children: [
            { path: 'books', component: AdBooksComponent },
            { path: 'books/:message', component: AdBooksComponent },
            { path: 'newspapers', component: AdNewspapersComponent },
            { path: 'magazines', component: AdMagazinesComponent },
            { path: 'publishers', component: AdPublishersComponent },
            { path: 'publisherEdit/:id', component: AdPublisherChangerComponent },
            { path: 'authors', component: AdAuthorsComponent },
            { path: 'authors/:message', component: AdAuthorsComponent },
            { path: 'authorEdit/:id', component: AdAuthorChangerComponent },
            { path: 'createAuthor', component: AdAuthorChangerComponent },
            { path: 'createPublisher', component: AdPublisherChangerComponent },
            { path: 'bookEdit/:id', component: AdBookChangerComponent },
            { path: 'magazineEdit/:id', component: AdMagazineChangerComponent },
            { path: 'newspaperEdit/:id', component: AdNewspaperChangerComponent },
            { path: 'createBook', component: AdBookChangerComponent },
            { path: 'createMagazine', component: AdMagazineChangerComponent },
            { path: 'createNewspaper', component: AdNewspaperChangerComponent }
        ]
    }
]);