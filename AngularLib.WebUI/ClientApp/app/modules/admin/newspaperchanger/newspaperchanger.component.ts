import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ShortPublisher } from '../../../viewModels/publisherViewModels/shortPublisherView';
import { Newspaper } from '../../../viewModels/newspaperViewModels/newspaperView';
import { NewspaperService } from '../../../services/newspaper.service';
import { PublisherService } from '../../../services/publisher.service';


@Component({
    selector: 'ad-newspaperchanger',
    templateUrl: './newspaperchanger.component.html',
})
export class AdNewspaperChangerComponent implements OnInit {
    title: string = "Create";
    id: number;
    errorMessage: any;
    isRequesting: boolean;
    newspaper: Newspaper;

    publishers: ShortPublisher[];

    constructor(private _avRoute: ActivatedRoute,
        private _newspaperService: NewspaperService,
        private _publisherService: PublisherService,
        private _router: Router) {

        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }

        this.newspaper = new Newspaper();

    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._newspaperService.getNewspaperById(this.id)
                .subscribe(resp => this.newspaper = resp
                , error => this.errorMessage = error);
        }

        this._publisherService.getShortPublishers()
            .subscribe(publishers => this.publishers = publishers,
            error => this.errorMessage = error);


    }

    save() {
        this.isRequesting = true;
        if (this.title == "Create") {
            this._newspaperService.saveNewspaper(this.newspaper).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/newspapers']);
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._newspaperService.updateNewspaper(this.newspaper).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/newspapers']);
                }, error => this.errorMessage = error)

        }
    }

    cancel() {
        this._router.navigate(['/admin/newspapers']);

    }

}


