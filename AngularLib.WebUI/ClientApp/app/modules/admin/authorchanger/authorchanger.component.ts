import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Author } from '../../../viewModels/authorViewModels/authorView';
import { AuthorService } from '../../../services/author.service';



@Component({
    selector: 'ad-authorchanger',
    templateUrl: './authorchanger.component.html',
    styleUrls: ['../changer.component.css']
})
export class AdAuthorChangerComponent implements OnInit {
    title: string = "Create";
    id: number;
    errorMessage: any;
    author: Author;
    isRequesting: boolean;

    constructor(private _avRoute: ActivatedRoute,
        private _authorService: AuthorService, private _router: Router) {
        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }
        this.author = new Author(); 
    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._authorService.getAuthorById(this.id)
                .subscribe(resp => this.author = resp
                , error => this.errorMessage = error);
        }
    }

    save() {
        this.isRequesting = true;
        if (this.title == "Create") {
            this._authorService.saveAuthor(this.author)
                .finally(() => this.isRequesting = false)
                .subscribe(value => {
                    if ((value as boolean) == false) {
                        let message = "The author was not created"
                        this._router.navigate(["/admin/authors/" + message]);
                    }
                    else {
                        let message = "The author was created"
                        this._router.navigate(["/admin/authors/" + message]);
                    }
                    
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._authorService.updateAuthor(this.author)
                .finally(() => this.isRequesting = false)
                .subscribe(value => {
                    if ((value as boolean) == false) {
                        let message = "The author was not editing"
                        this._router.navigate(["/admin/authors/" + message]);
                    }
                    else {
                        let message = "The author was editing"
                        this._router.navigate(["/admin/authors/" + message]);
                    }
                    
                }, error => this.errorMessage = error)
        }
    }

  


    cancel() {
        this._router.navigate(['/admin/authors']);
        
    }

}


