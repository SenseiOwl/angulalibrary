import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorService } from '../../../services/author.service';
import { PaginatedAuthors } from '../../../viewModels/authorViewModels/paginatedBookView';



@Component({
    selector: 'ad-authors',
    templateUrl: './authors.component.html'
})
export class AdAuthorsComponent {
    public authors: PaginatedAuthors;
    public message: string;

    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _authorService: AuthorService, private _acRouter: ActivatedRoute, private _router : Router) {
        
    }

    ngOnInit() {
        this.getAuthors(this.CurrentPage);
        if (this._acRouter.snapshot.params["message"]) {
            this.message = this._acRouter.snapshot.params["message"];
        }
    }

    getAuthors(page: number) {
        this._authorService.getPaginatedAuthors(page, this.itemPerPage).subscribe(result => {
            this.authors = result;
        }, error => console.error(error));
    }

    delete(ID: any) {
        var ans = confirm("Do you want to delete this book?");
        if (ans) {
            this._authorService.deleteAuhtor(ID).subscribe((value) => {
                if ((value as boolean) == false) {
                    let massage = "The author was not deleted"
                    this.getAuthors(this.CurrentPage);

                }
                else {
                    let message = "The author was deleted"
                    this.getAuthors(this.CurrentPage);
                }
                
            }, error => console.error(error))
        }
    }


    onMessageClick() {
        this.message = "";
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getAuthors(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getAuthors(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}
 
