import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Book } from '../../../viewModels/bookViewModels/bookView';
import { ShortAuthor } from '../../../viewModels/authorViewModels/shortAuhtorView';
import { ShortPublisher } from '../../../viewModels/publisherViewModels/shortPublisherView';
import { BookService } from '../../../services/book.service';
import { AuthorService } from '../../../services/author.service';
import { PublisherService } from '../../../services/publisher.service';



@Component({
    selector: 'ad-bookchanger',
    templateUrl: './bookchanger.component.html',
    styleUrls: ['../changer.component.css']
})
export class AdBookChangerComponent implements OnInit {
    title: string = "Create";
    id: number;
    isRequesting: boolean;
    errorMessage: any;

    book: Book;

    authors: ShortAuthor[];
    publishers: ShortPublisher[];

    constructor(private _avRoute: ActivatedRoute,
        private _bookService: BookService,
        private _authorService: AuthorService,
        private _publisherService: PublisherService,
        private _router: Router) {

        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }

        this.book = new Book();

    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._bookService.getBookById(this.id)
                .subscribe(resp => this.book = resp
                , error => this.errorMessage = error);
        }

        this._authorService.getShortAuthors()
            .subscribe(authors => this.authors = authors,
            error => this.errorMessage = error);

        this._publisherService.getShortPublishers()
            .subscribe(publishers => this.publishers = publishers,
            error => this.errorMessage = error);


    }

    save() {
        this.isRequesting = true;
        if (this.title == "Create") {
            this._bookService.saveBook(this.book)
                .finally(() => this.isRequesting = false)
                .subscribe(value => {
                    if ((value as boolean) == false) {
                        let message = "The book was not created"
                        this._router.navigate(["/admin/books/" + message]);
                    }
                    else {
                        let message = "The book was created"
                        this._router.navigate(["/admin/books/" + message]);
                    }
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._bookService.updateBook(this.book)
                .finally(() => this.isRequesting = false)
                .subscribe(value => {
                    if ((value as boolean) == false) {
                        let message = "The book was not edited"
                        this._router.navigate(["/admin/books/'" + message]);
                    }
                    else {
                        let message = "The book was edited"
                        this._router.navigate(["/admin/books/" + message]);
                    }
                }, error => this.errorMessage = error)

        }
    }

    cancel() {
        this._router.navigate(['/admin/books']);

    }

}




