﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './library.routing';
import { BooksComponent } from './books/books.component';
import { NewspapersComponent } from './newspapers/newspapers.component';
import { MagazinesComponent } from './magazines/magazines.component';
import { AuthorComponent } from './author/author.component';
import { PublisherComponent } from './publisher/publisher.component';
import { BookComponent } from './book/book.component';
import { HoverDirective } from '../../directives/hover.directive';
import { NewspaperComponent } from './newspaper/newspaper.component';
import { MagazineComponent } from './magazine/magazine.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule, FormsModule,SharedModule, routing
    ],
    declarations: [

        BooksComponent,
        NewspapersComponent,
        MagazinesComponent,
        AuthorComponent,
        PublisherComponent,
        BookComponent,
        HoverDirective,
        NewspaperComponent,
        MagazineComponent
    ],
    providers: []
})
export class LibraryModule { }