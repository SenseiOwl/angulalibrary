import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedNewspaper } from '../../../viewModels/newspaperViewModels/paginatedNewspaper';
import { Newspaper } from '../../../viewModels/newspaperViewModels/newspaperView';
import { NewspaperService } from '../../../services/newspaper.service';

@Component({
    selector: 'newspapers',
    templateUrl: './newspapers.component.html',
    styleUrls: ['./newspapers.component.css']
})
export class NewspapersComponent implements OnInit{
    
    public newspapers: PaginatedNewspaper;
    selectedNewspaper: Newspaper;

    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _newspaperService : NewspaperService) {
        
    }

    ngOnInit(): void {
        this.getNewspapers(this.CurrentPage);
    }

    getNewspapers(page: number) {
        this._newspaperService.getPaginatedNewspapers(page, this.itemPerPage)
            .subscribe(newspapers => this.newspapers = newspapers);
    }

    onClick(newspaper: Newspaper) {
        this.selectedNewspaper = newspaper;
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getNewspapers(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getNewspapers(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}



