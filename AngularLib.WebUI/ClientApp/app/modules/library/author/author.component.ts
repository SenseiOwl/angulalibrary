import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Author } from '../../../viewModels/authorViewModels/authorView';
import { AuthorService } from '../../../services/author.service';


@Component({
    selector: 'app-author',
    templateUrl: './author.component.html',
    styleUrls: ['./author.component.css']
})
export class AuthorComponent {

    author: Author;
    selectedBookId: number;

    constructor(
        private _route: ActivatedRoute,
        private _authorService: AuthorService,
        private _location: Location
    ) { }

    ngOnInit(): void {
        this.getAuthor();
    }

    onClick(id: number) {
        this.selectedBookId = id;
    }


    getAuthor(): void {
        const id = this._route.snapshot.params['id'];
        this._authorService.getAuthorById(id)
            .subscribe(author => this.author = author);
    }

    goBack(): void {
        this._location.back();
    }

}


