import { Component, Input } from '@angular/core';
import { Book } from '../../../viewModels/bookViewModels/bookView';
import { BookService } from '../../../services/book.service';

@Component({
    selector: 'app-book',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.css']
})
export class BookComponent{

    @Input() book: Book;

    @Input()
    set bookId(bookId: number) {
        this._bookService.getBookById(bookId).subscribe(book => this.book = book);
    }

    constructor(private _bookService:BookService) {

    }

    close() {
        this.book = new Book();
    }
}

