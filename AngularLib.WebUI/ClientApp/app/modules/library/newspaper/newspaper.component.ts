import { Component, Input } from '@angular/core';
import { NewspaperService } from '../../../services/newspaper.service';
import { Newspaper } from '../../../viewModels/newspaperViewModels/newspaperView';


@Component({
    selector: 'app-newspaper',
    templateUrl: './newspaper.component.html',
    styleUrls: ['./newspaper.component.css']
})
export class NewspaperComponent {

    @Input() newspaper: Newspaper;

    @Input()
    set newspaperId(newspaperId: number) {
        this._newspaperService.getNewspaperById(newspaperId).subscribe(newspaper => this.newspaper = newspaper);
    }

    constructor(private _newspaperService : NewspaperService) {

    }

    close() {
        this.newspaper = new Newspaper();
    }
}

