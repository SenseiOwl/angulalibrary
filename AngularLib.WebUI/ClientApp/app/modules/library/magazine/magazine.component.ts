import { Component, Input } from '@angular/core';
import { Magazine } from '../../../viewModels/magazineViewModels/magazineView';
import { MagazineService } from '../../../services/magazine.service';

@Component({
    selector: 'app-magazine',
    templateUrl: './magazine.component.html',
    styleUrls: ['./magazine.component.css']
})
export class MagazineComponent {

    @Input() magazine: Magazine;

    @Input()
    set magazineId(magazineId: number) {
        this._magazineService.getMagazineById(magazineId).subscribe(magazine => this.magazine = magazine);
    }

    constructor(private _magazineService:MagazineService) {

    }

    close() {
        this.magazine = new Magazine();
    }
}

