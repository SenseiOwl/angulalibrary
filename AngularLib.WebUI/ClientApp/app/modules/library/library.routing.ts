﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { MagazinesComponent } from './magazines/magazines.component';
import { NewspapersComponent } from './newspapers/newspapers.component';
import { AuthorComponent } from './author/author.component';
import { PublisherComponent } from './publisher/publisher.component';

export const routing: ModuleWithProviders = RouterModule.forChild([
    { path: 'books', component: BooksComponent },
    { path: 'magazines', component: MagazinesComponent },
    { path: 'newspapers', component: NewspapersComponent, },
    { path: 'author/:id', component: AuthorComponent },
    { path: 'publisher/:id', component: PublisherComponent },
]);