import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedBook } from '../../../viewModels/bookViewModels/paginatedBookView';
import { Book } from '../../../viewModels/bookViewModels/bookView';
import { BookService } from '../../../services/book.service';

@Component({
    selector: 'books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.css']
})
export class BooksComponent {
    public paginatedBooks: PaginatedBook;
    selectedBook: Book;

    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _bookService: BookService) {
        
    }

    ngOnInit() {
        this.getBooks(this.CurrentPage);
    }

    getBooks(page:number) {

        this._bookService.getPaginatedBooks(page, this.itemPerPage).subscribe(
            result => {
                this.paginatedBooks = result;
            }, error => console.error(error));   
    }

    onClick(book: Book):void {
        this.selectedBook = book;
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }   
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getBooks(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getBooks(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}


