﻿import { ShortBook } from "../bookViewModels/shortBookView";
import { ShortMagazine } from "../magazineViewModels/shortMagazine";
import { ShortNewspaper } from "../newspaperViewModels/shortNewspaper";



export class Publisher {
    id: number;
    yearOfOpening: number;
    name: string;
    books: ShortBook[];
    magazines: ShortMagazine[];
    newspapers: ShortNewspaper[];
}