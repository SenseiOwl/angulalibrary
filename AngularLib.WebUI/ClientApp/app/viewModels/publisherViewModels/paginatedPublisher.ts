﻿import { Publisher } from "./publisherView";
import { PageInfo } from "../pageInfo";

export class PaginatedPublisher {
    public publishers: Publisher[];
    public pageInfo: PageInfo;
}