﻿import { PageInfo } from "../pageInfo";
import { Book } from "./bookView";

export class PaginatedBook {
    public books: Book[];
    public pageInfo: PageInfo;
}