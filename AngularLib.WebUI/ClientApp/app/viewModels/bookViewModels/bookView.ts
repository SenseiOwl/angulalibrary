﻿import { ShortPublisher } from "../publisherViewModels/shortPublisherView";
import { ShortAuthor } from "../authorViewModels/shortAuhtorView";


export class Book {
    id: number;
    name: string;
    yearOfWriting: number;
    yearOfPublishing: number;
    publisher: ShortPublisher[];
    authors: ShortAuthor[];
}