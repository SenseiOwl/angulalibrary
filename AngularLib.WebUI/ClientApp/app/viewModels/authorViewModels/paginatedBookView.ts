﻿import { Author } from "./authorView";
import { PageInfo } from "../pageInfo";

export class PaginatedAuthors {

    public authors: Author[];
    public pageInfo: PageInfo;

}