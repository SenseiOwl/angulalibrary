﻿import { Magazine } from "./magazineView";
import { PageInfo } from "../pageInfo";

export class PaginatedMagazine {
    public magazines: Magazine[];
    public pageInfo: PageInfo;
}