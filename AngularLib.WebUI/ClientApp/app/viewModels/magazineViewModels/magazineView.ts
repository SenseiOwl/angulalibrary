﻿import { ShortPublisher } from "../publisherViewModels/shortPublisherView";

export class Magazine {
    id: number;
    yearOfPublishing: number;
    name: string;
    publisher: ShortPublisher[];
}