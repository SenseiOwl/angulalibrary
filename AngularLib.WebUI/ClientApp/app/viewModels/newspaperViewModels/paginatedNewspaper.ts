﻿import { Newspaper } from "./newspaperView";
import { PageInfo } from "../pageInfo";

export class PaginatedNewspaper {
    public newspapers: Newspaper[];
    public pageInfo: PageInfo;
}