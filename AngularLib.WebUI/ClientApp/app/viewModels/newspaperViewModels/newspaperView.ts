﻿import { ShortPublisher } from "../publisherViewModels/shortPublisherView";

export class Newspaper {
    id: number;
    yearOfPublishing: number;
    name: string;
    publisher: ShortPublisher[];
}