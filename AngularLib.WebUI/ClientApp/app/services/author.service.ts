﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PaginatedAuthors } from '../viewModels/authorViewModels/paginatedBookView';
import { AllAuthors } from '../viewModels/authorViewModels/allAuthorView';
import { ShortAuthor } from '../viewModels/authorViewModels/shortAuhtorView';
import { Author } from '../viewModels/authorViewModels/authorView';
import { BaseService } from './base.service';
import { UserService } from './user.service';

@Injectable()
export class AuthorService extends BaseService{
    myAppUrl: string = "";

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string, private _user : UserService) {
        super();
        this.myAppUrl = baseUrl;
    }
    
    getAuthors(): Observable<AllAuthors> {
        return this._http.get(this.myAppUrl + 'api/Author/GetAll')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getPaginatedAuthors(page: number, size: number): Observable<PaginatedAuthors> {
        return this._http.get(this.myAppUrl + "api/Author/GetPaginated/" + page + "/" + size)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getShortAuthors(): Observable<ShortAuthor[]> {
        return this._http.get(this.myAppUrl + 'api/Author/GetShortAuthors')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    } 

    getAuthorById(id: number) : Observable<Author> {
        return this._http.get(this.myAppUrl + "api/Author/Get/" + id)
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    saveAuthor(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.post(this.myAppUrl + 'api/Author/Create', author, { headers })
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    updateAuthor(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.put(this.myAppUrl + 'api/Author/Edit', author, { headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    deleteAuhtor(id: number): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.delete(this.myAppUrl + "api/Author/Delete/" + id, { headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

}