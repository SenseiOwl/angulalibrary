﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PaginatedNewspaper } from '../viewModels/newspaperViewModels/paginatedNewspaper';
import { AllNewspaper } from '../viewModels/newspaperViewModels/allNewspaperView';
import { Newspaper } from '../viewModels/newspaperViewModels/newspaperView';
import { BaseService } from './base.service';
import { UserService } from './user.service';

@Injectable()
export class NewspaperService extends BaseService{
    myAppUrl: string = "";

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string, private _user: UserService) {
        super();
        this.myAppUrl = baseUrl;
    }

    getNewspapers(): Observable<AllNewspaper> {
        return this._http.get(this.myAppUrl + 'api/Newspaper/GetAll')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getNewspaperById(id: number): Observable<Newspaper> {
        return this._http.get(this.myAppUrl + "api/Newspaper/Get/" + id)
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    getPaginatedNewspapers(page: number, size: number): Observable<PaginatedNewspaper> {
        return this._http.get(this.myAppUrl + "api/Newspaper/GetPaginated/" + page + "/" + size)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    saveNewspaper(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.post(this.myAppUrl + 'api/Newspaper/Create', author, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    updateNewspaper(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.put(this.myAppUrl + 'api/Newspaper/Edit', author, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    deleteNewspaper(id: number): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.delete(this.myAppUrl + "api/Newspaper/Delete/" + id, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

}