﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AllBook } from '../viewModels/bookViewModels/allBookView';
import { PaginatedBook } from '../viewModels/bookViewModels/paginatedBookView';
import { Book } from '../viewModels/bookViewModels/bookView';
import { UserService } from './user.service';

@Injectable()
export class BookService {
    myAppUrl: string = "";

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string, private _user : UserService) {
        this.myAppUrl = baseUrl;
    }

    getBooks(): Observable<AllBook> {
        
        return this._http.get(this.myAppUrl + 'api/Book/GetAll')
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    getBookById(id: number): Observable<Book> {
        return this._http.get(this.myAppUrl + "api/Book/Get/" + id)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }

    getPaginatedBooks(page: number, size: number): Observable<PaginatedBook> {

        return this._http.get(this.myAppUrl + "api/Book/GetPaginated/" + page + "/" + size)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    saveBook(author: any): Observable<boolean> {

        let headers : Headers = this._user.getTokenHeader();
        return this._http.post(this.myAppUrl + 'api/Book/Create', author, { headers })
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }

    updateBook(author: any): Observable<boolean> {

        let headers: Headers = this._user.getTokenHeader();
        return this._http.put(this.myAppUrl + 'api/Book/Edit', author, { headers })
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    deleteBook(id: number): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.delete(this.myAppUrl + "api/Book/Delete/" + id, { headers })
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        console.log(error);
        return Observable.throw(error);
    }
}