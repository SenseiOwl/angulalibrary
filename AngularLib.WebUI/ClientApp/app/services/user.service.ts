﻿import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { BehaviorSubject } from "rxjs";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { UserRegistration } from "../viewModels/authViewModels/user.registration";
import { ConfigService } from "./config.service";
import { CookieService } from "./cookie.service";

@Injectable()

export class UserService extends BaseService {

    baseUrl: string = '';

    private _authNavStatusSource = new BehaviorSubject<boolean>(false);
    authNavStatus$ = this._authNavStatusSource.asObservable();

    private loggedIn = false;

    constructor(private _http: Http, private _configService: ConfigService, private _cookie: CookieService) {
        super();
        this.loggedIn = !!this._cookie.getCookie('auth_token');
        this._authNavStatusSource.next(this.loggedIn);
        this.baseUrl = _configService.getApiURI();
    }

    register(email: string, password: string, firstName: string, lastName: string, location: string): Observable<UserRegistration> {
        let body = JSON.stringify({ email, password, firstName, lastName, location });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.baseUrl + "/accounts/post", body, options)
            .map(res => true)
            .catch(this.handleError);
    }

    login(userName, password) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http
            .post(
            this.baseUrl + '/auth/login',
            JSON.stringify({ userName, password }), { headers }
            )
            .map(res => res.json())
            .map(res => {
                this._cookie.setCookie('auth_token', res.auth_token, res.expires_in);
                this._cookie.setCookie('role', res.role, res.expires_in)
                this.loggedIn = true;
                this._authNavStatusSource.next(true);
                return true;
            })
            .catch(this.handleError);
    }

    logout() {
        this._cookie.deleteCookie('auth_token');
        this._cookie.deleteCookie('role');
        this.loggedIn = false;
        this._authNavStatusSource.next(false);
    }

    isLoggedIn() {
        return this.loggedIn;
    }

    inRole(name: string): boolean {
        let role = this._cookie.getCookie('role');
        if (role == undefined) {
            return false;
        }
        if (role == name) {
            return true;
        }
        return false;
    }

    getTokenHeader() : Headers {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let authToken = this._cookie.getCookie('auth_token');
        headers.append('Authorization', `Bearer ${authToken}`);
        return headers;
    }

}