﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PaginatedMagazine } from '../viewModels/magazineViewModels/paginatedMagazineView';
import { AllMagazine } from '../viewModels/magazineViewModels/allMagazineView';
import { Magazine } from '../viewModels/magazineViewModels/magazineView';
import { BaseService } from './base.service';
import { UserService } from './user.service';


@Injectable()
export class MagazineService extends BaseService{
    myAppUrl: string = "";

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string, private _user : UserService) {
        super();
        this.myAppUrl = baseUrl;
    }

    getMagazines(): Observable<AllMagazine>{
        return this._http.get(this.myAppUrl + 'api/Magazine/GetAll')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getMagazineById(id: number): Observable<Magazine> {
        return this._http.get(this.myAppUrl + "api/Magazine/Get/" + id)
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    getPaginatedMagazines(page: number, size: number): Observable<PaginatedMagazine> {
        return this._http.get(this.myAppUrl + "api/Magazine/GetPaginated/" + page + "/" + size)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    saveMagazine(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.post(this.myAppUrl + 'api/Magazine/Create', author, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    updateMagazine(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.put(this.myAppUrl + 'api/Magazine/Edit', author, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    deleteMagazine(id: number): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.delete(this.myAppUrl + "api/Magazine/Delete/" + id, {headers})
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

}
