import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy{
    status: boolean;

    authSubscription: Subscription;


    constructor(private _userService: UserService) {
    }

    logout() {
        this._userService.logout();
    }

    ngOnInit() {
        this.authSubscription = this._userService.authNavStatus$.subscribe(status => this.status = status);

    }

    ngOnDestroy() {
        // prevent memory leak when component is destroyed
        this.authSubscription.unsubscribe();

    }

    isAdmin() {
        return this._userService.inRole('admin');
    }
}
