﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AngularLib.BusinessLogic.Services;
using AngularLib.ViewModels.AuthorViewModels;
using Microsoft.AspNetCore.Authorization;
using AngularLib.BusinessLogic.Common;

namespace AngularLib.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class AuthorController : Controller
    {
        private AuthorService _service;

        public AuthorController(AuthorService service)
        {
            _service = service;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll()
        {

            try
            {
                var result = await _service.GetAll();
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> GetShortAuthors()
        {
            try
            {
                var result = await _service.GetShortAuthors();
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }

        [HttpGet("[action]/{page}/{size}")]
        public async Task<IActionResult> GetPaginated(int page, int size)
        {
            try
            {
                var result = await _service.GetPaginated(page,size);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }

        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var result = await _service.Get(id);
                return Ok(result);
            }
            catch (BusinessLogicException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpPost("[action]")]
        public async Task<IActionResult> Create([FromBody] CreateAuthorViewModel author)
        {
            try
            {
                var result = await _service.Create(author);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpPut("[action]")]
        public async Task<IActionResult> Edit([FromBody] UpdateAuthorViewModel author)
        {
            try
            {
                var result = await _service.Update(author);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var result = await _service.Delete(id);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }

    }
}